# smart-balancing-NL-BEL-GER-data-analysis-2015-2019

Python scripts related to the manuscript "Smart Balancing of electrical power - Matching Market Rules with Balancing Requirements". Single author publication by Felix Röben (with the aim to obtain the doctoral degree). Preprint submitted to Elsevier
